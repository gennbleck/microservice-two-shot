# Wardrobify

Team:

* Person 1 - Which microservice?
Christopher Byrne - Hats Microservice

* Person 2 - Which microservice?
* Dane Watt - Shoes Microservice

## Design

## Shoes microservice

Created ShoeModel that contained all the characteristics needed for the shoe, and a BinVO model that inhereted characteristics from the Bin model that was already made. Made a few encoders for the list view and detail view of the shoe model to use in my api methods. 



## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The only models I made were Hats and a Location Value Object.

The Hats model is dependent on the LocationVO for its location property, which includes the href (and closet name, for a bonus). I chose not to use the location ID as the reference for locations when creating hats due to DDD.

The LocationVO has its attributes populated by the poller, which pulls from the list of created Location objects (via api reference). The Location models themselves were included within the wardrobe API.
