import React, { useState, useEffect } from 'react';

function ShoeForm(props) {
    const [shoeManufacturer, setShoeManufacturer] = useState('');
    const [shoeModel, setShoeModel] = useState('');
    const [shoeColor, setShoeColor] = useState('');
    const [shoePhoto, setShoePhoto] = useState('');
    const [shoeBin, setBin] = useState('');
    const [shoeBins, setBins] = useState([]);

    useEffect(() => {
        async function fetchBins() {
            const url = 'http://localhost:8100/api/bins/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setBins(data.shoeBins);
            }
        }
        fetchBins();
    }, [])



    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            manufacturer: shoeManufacturer,
            model: shoeModel,
            color: shoeColor,
            photo: shoePhoto,
            bin: shoeBin,
        };

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig):
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setShoeManufacturer('');
            setShoeModel('');
            setShoeColor('');
            setShoePhoto('');
            setBin('');
        }
    }

    function handleManufacturerChange(event) {
        const value = event.target.value;
        setShoeManufacturer(value);
    }

    function handleModelChange(event) {
        const value = event.target.value;
        setShoeModel(value);
    }

    function handleColorChange(event) {
        const value = event.target.value;
        setShoeColor(value);
    }

    function handlePhotoChange(event) {
        const value = event.target.value;
        setShoePhoto(value);
    }

    function handleBinChange(event) {
        const value = event.target.value;
        setBin(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={roomCount} onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                            <label htmlFor="room_count">Room count</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={city} onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                            <label htmlFor="city">City</label>
                        </div>
                        <div className="mb-3">
                            <select value={state} onChange={handleStateChange} required name="state" id="state" className="form-select">
                                <option value="">Choose a state</option>
                                {states.map(state => {
                                    return (
                                        <option key={state.abbreviation} value={state.abbreviation}>
                                            {state.name}
                                        </option>
                        );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
    )


}
