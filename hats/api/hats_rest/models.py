from django.db import models

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.name}"

class Hat(models.Model):
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    img_url = models.URLField()
    fabric = models.CharField(max_length=200, null=True)
    style = models.CharField(max_length=200, null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.PROTECT,
        null=True
    )

    def __str__(self):
        return f"{self.name}"
