from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

# Create your views here.

from .models import LocationVO, Hat


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "import_href"
    ]

class HatdetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "color",
        "img_url",
        "fabric",
        "style",
        "location"
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:

            location = LocationVO.objects.get(import_href=content["location"])

            content["location"] = location


            print(content)

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatdetailEncoder,
            safe=False,
        )

    else:
        hat = Hat.objects.all()
        return JsonResponse(
            {"hats": hat},
            encoder=HatListEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_hat_details(request, id):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatdetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Invalid ID"}, status=400)


@require_http_methods(["GET"])
def api_list_styles(request):
    styles = Style.objects.all()
    return JsonResponse({"styles": styles}, encoder=StyleEncoder, safe=False)


@require_http_methods(["GET"])
def api_list_fabrics(request):
    fabrics = Fabric.objects.all()
    return JsonResponse({"fabrics": fabrics}, encoder=FabricEncoder, safe=False)
