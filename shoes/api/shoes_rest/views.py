from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json
# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "name", "number"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "bin"
    ]
    encoders={"bin": BinVOEncoder(),}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "photo",
        "bin"
    ]

    encoders={"bin": BinVOEncoder(),}



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        # if bin_vo_id is not None:
        #     shoe = Shoe.objects.filter(bin=bin_vo_id)

        shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoe},
            encoder = ShoeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,

        )

@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    if request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    # else:
    #     try:
    #         content = json.loads(request.body)
    #         shoe = Shoe.objects.get(id=pk)

    #         props = ["manufacturer", "model_name", "color", "photo", "bin"]
    #         for prop in props:
    #             if prop in content:
    #                 setattr(shoe, prop, content[prop])
    #         shoe.save()
    #         return JsonResponse(
    #             shoe,
    #             encoder=ShoeDetailEncoder,
    #             safe=False,
    #         )
    #     except Shoe.DoesNotExist:
    #         response = JsonResponse({"message": "Does not exist"})
    #         response.status_code = 404
    #         return response
def api_list_binvo(request):
    bins = BinVO.objects.all()
    return JsonResponse(
        bins,
        encoder=BinVOEncoder,
        safe=False
    )
