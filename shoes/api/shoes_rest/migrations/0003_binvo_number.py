# Generated by Django 4.0.3 on 2023-06-03 00:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_binvo_shoe_photo_shoe_bin'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
