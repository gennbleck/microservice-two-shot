from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200, null=True)
    number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return f'{self.name}'

    # This may be causing problems

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=20)
    photo = models.URLField(max_length=200, null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
        null=True
    )
    def __str__(self):
        return f'{self.manufacturer} | {self.model_name}'

    def get_api_url(self):
        return reverse("api_list_shoes", kwargs={"pk": self.pk})
