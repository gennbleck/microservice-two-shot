from django.urls import path
from .views import api_list_shoes, api_show_shoe, api_list_binvo

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    # path("shoes/<int:pk>/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_show_shoe, name="api_show_shoe"),
    path("bins/", api_list_binvo, name="api_list_binvo")

]
